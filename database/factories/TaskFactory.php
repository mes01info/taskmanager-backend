<?php

/** @var Factory $factory */

use App\Model;
use App\Models\Task;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30),
        'description' => $faker->text(),
        'status' => $faker->randomElement(array_keys(Task::STATUSES)),
    ];
});
