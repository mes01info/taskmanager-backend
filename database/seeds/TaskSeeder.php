<?php

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Task::class, 30)->create()->each(function (Task $task) {
            /** @var User $user */
            $user = User::inRandomOrder()->first();

            $task->users()->attach($user);
            $task->save();
        });
    }
}
