##Установка

1. `composer install`
2. `cp .env.example .env` -> после в .env настроить подключение к БД
3. `php artisan key:generate`
4. `php artisan migrate --seed`
5. `php artisan storage:link`


##Использование

 - API доступно по адресу: `http://YOUR_DOMAIN/api`
 - [Postman](https://www.postman.com/) коллекция `api.postman_collection.json`
