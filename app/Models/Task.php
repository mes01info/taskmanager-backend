<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int id
 * @property string title
 * @property string description
 * @property string status
 * @property string updated_at
 * @property string created_at
 * @property-read User[]|Collection users
 * @method Task search(string $q)
 * @mixin Builder
 * */
class Task extends Model
{
    public const STATUSES = [
      'wait' => 'В ожидании',
      'work' => 'В работе',
      'done' => 'Готова',
    ];

    /**
     * @param $query
     * @param string $q
     * @return mixed
     */
    public function scopeSearch($query, string $q)
    {
        return $query->whereRaw('MATCH (title,description) AGAINST (? IN BOOLEAN MODE)', [$q]);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'task_user');
    }
}
