<?php

namespace App\Http\Requests\Api;

use App\Models\Task;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $statusCodes = array_keys(Task::STATUSES);

        return [
            'id' => 'required|exists:tasks,id',
            'title' => 'required',
            'description' => 'required',
            'status' => 'required|in:'.implode(',', $statusCodes),
            'users.*' => 'nullable|exists:users,id',
        ];
    }
}
