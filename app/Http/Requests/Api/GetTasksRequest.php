<?php

namespace App\Http\Requests\Api;

use App\Models\Task;
use Illuminate\Foundation\Http\FormRequest;

class GetTasksRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $statusCodes = array_keys(Task::STATUSES);

        return [
            'search' => 'nullable',
            'status' => 'nullable|in:'.implode(',', $statusCodes),
            //todo: добавить валидацию для пагинации
        ];
    }
}
