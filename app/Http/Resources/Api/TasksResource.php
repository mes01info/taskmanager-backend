<?php

namespace App\Http\Resources\Api;

use App\Models\Task;
use Illuminate\Http\Resources\Json\JsonResource;

class TasksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Task[] $users */
        $tasks = $this->resource;
        $tasksArr = [];


        foreach ($tasks as $task) {
            $tasksArr[] = (new TaskResource($task))->process();
        }

        return [
            'list' => $tasksArr,
            'pagination' => [
                'total' => $tasks->total(),
                'count' => $tasks->count(),
                'per_page' => $tasks->perPage(),
                'current_page' => $tasks->currentPage(),
                'total_pages' => $tasks->lastPage()
            ],
        ];
    }
}
