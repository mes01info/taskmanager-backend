<?php

namespace App\Http\Resources\Api;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User[] $users */
        $users = $this->resource;
        $usersArr = [];


        foreach ($users as $user) {
            $usersArr[] = (new UserResource($user))->process();
        }

        return [
            'list' => $usersArr,
            'pagination' => [
                'total' => $users->total(),
                'count' => $users->count(),
                'per_page' => $users->perPage(),
                'current_page' => $users->currentPage(),
                'total_pages' => $users->lastPage()
            ],
        ];
    }
}
