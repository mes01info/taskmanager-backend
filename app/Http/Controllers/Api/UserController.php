<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateUserRequest;
use App\Http\Requests\Api\GetUsersRequest;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Http\Resources\Api\UserResource;
use App\Http\Resources\Api\UsersResource;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param CreateUserRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function createUser(CreateUserRequest $request)
    {
        try {
            $user = $this->userService->createOrUpdateUser($request);
            return new UserResource($user);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка создания'], 500);
        }
    }

    /**
     * @param UpdateUserRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function updateUser(UpdateUserRequest $request)
    {
        try {
            $task = $this->userService->createOrUpdateUser($request);
            return new UserResource($task);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка сохранения'], 500);
        }
    }

    /**
     * @param GetUsersRequest $request
     * @return UsersResource
     */
    public function getUsers(GetUsersRequest $request): UsersResource
    {
        $tasks = $this->userService->getUsers($request);
        return new UsersResource($tasks);
    }
}
