<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateTaskRequest;
use App\Http\Requests\Api\GetTasksRequest;
use App\Http\Requests\Api\UpdateTaskRequest;
use App\Http\Resources\Api\TaskResource;
use App\Http\Resources\Api\TasksResource;
use App\Services\TaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * @var TaskService
     */
    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param CreateTaskRequest $request
     * @return TaskResource|\Illuminate\Http\JsonResponse
     */
    public function createTask(CreateTaskRequest $request)
    {
        try {
            $task = $this->taskService->createOrUpdateTask($request);
            return new TaskResource($task);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка создания'], 500);
        }
    }

    /**
     * @param UpdateTaskRequest $request
     * @return TaskResource|\Illuminate\Http\JsonResponse
     */
    public function updateTask(UpdateTaskRequest $request)
    {
        try {
            $task = $this->taskService->createOrUpdateTask($request);
            return new TaskResource($task);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка сохранения'], 500);
        }
    }

    /**
     * @param GetTasksRequest $request
     * @return TasksResource
     */
    public function getTasks(GetTasksRequest $request): TasksResource
    {
        $tasks = $this->taskService->getTasks($request);
        return new TasksResource($tasks);
    }
}
