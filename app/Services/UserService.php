<?php


namespace App\Services;


use App\Models\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserService
{
    protected $avatarSize = [70, 70];

    /**
     * @param Request $request
     * @return User
     * @throws Exception
     */
    public function createOrUpdateUser(Request $request): User
    {
        $data = $request->all();

        try {
            $user = !empty($data['id']) ? User::find($data['id']) : new User();

            $user->fio = $data['fio'];

            if(!empty($request->file('avatar'))){
                if(!empty($user->avatar)){
                    Storage::disk('public')->delete('avatars/'.$user->getRawOriginal('avatar'));
                }

                $user->avatar = $this->saveAvatar($request->file('avatar'));
            }

            $user->save();

            return $user;
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param UploadedFile|string $file
     * @return string
     */
    public function saveAvatar($file): string
    {
        $image = Image::make($file);
        $image->fit($this->avatarSize[0], $this->avatarSize[1])->encode('jpg');

        $name = Str::random().'.jpg';
        $path = 'avatars/'.$name;

        Storage::disk('public')->put($path, $image);

        return $name;
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function getUsers(Request $request): LengthAwarePaginator
    {
        $filter = $request->all();

        $users = User::query();

        if(!empty($filter['search'])) {
            $users->where('fio', 'LIKE', '%'.$filter['search'].'%');
        }

        $filter['currentPage'] = $filter['currentPage'] ?? 1;
        $filter['perPage'] = $filter['perPage'] ?? 10;

        return $users
            ->orderBy('id', 'desc')
            ->paginate($filter['perPage'], ['*'], 'page', $filter['currentPage']);
    }
}
