<?php


namespace App\Services;


use App\Models\Task;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class TaskService
{
    /**
     * @param Request $request
     * @return Task
     * @throws Exception
     */
    public function createOrUpdateTask(Request $request): Task
    {
        $data = $request->all();

        try {
            $task =  !empty($data['id']) ? Task::find($data['id']) : new Task();

            $task->title = $data['title'];
            $task->description = $data['description'];
            $task->status = $data['status'];
            $task->save();

            if(!empty($data['users'])) {
                $task->users()->sync($data['users']);
            }

            return $task;
        }
        catch (Exception $e) {
            dd($e);
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function getTasks(Request $request): LengthAwarePaginator
    {
        $filter = $request->all();

        $tasks = Task::query()->with(['users']);

        if(!empty($filter['search'])) {
            $tasks->search($filter['search']);
        }

        if(!empty($filter['status'])) {
            $tasks->where('status', $filter['status']);
        }

        $filter['currentPage'] = $filter['currentPage'] ?? 1;
        $filter['perPage'] = $filter['perPage'] ?? 10;

        return $tasks
            ->orderBy('id', 'desc')
            ->paginate($filter['perPage'], ['*'], 'page', $filter['currentPage']);
    }
}
