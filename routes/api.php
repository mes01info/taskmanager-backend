<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'task'], function () {
    Route::get('/list', 'TaskController@getTasks');
    Route::post('/create', 'TaskController@createTask');
    Route::post('/update', 'TaskController@updateTask');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/list', 'UserController@getUsers');
    Route::post('/create', 'UserController@createUser');
    Route::post('/update', 'UserController@updateUser');
});
